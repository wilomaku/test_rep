
from pedestrain import Pedestrain
from vector import Vector
import default_config
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import random


class Simulator(object):

    def __init__(self, total_pedestrains, env_start_position, env_end_position):
        self.pedestrains = []
        self.total_pedestrains = total_pedestrains
        self.env_start_position = env_start_position
        self.env_end_position = env_end_position

        self.gen_pedestrains(total_pedestrains)

    def gen_pedestrains(self, total_pedestrains):

        target_position = Vector(self.env_end_position.x,self.env_end_position.y//2)
        rev_target_position = Vector(self.env_start_position.x,self.env_end_position.y//2)
        pedestrain_id = 1
        for i in range(0,total_pedestrains):
            start_position = Vector(random.randint(self.env_start_position.x,self.env_end_position.x),
                             random.randint(self.env_start_position.y,self.env_end_position.y))

            type = random.randint(0,self.total_pedestrains)%2
            if(type==0):
                self.pedestrains.append(Pedestrain(start_position, target_position, pedestrain_id, 'b'))
            else:
                self.pedestrains.append(Pedestrain(start_position, rev_target_position, pedestrain_id, 'r'))

            pedestrain_id += 1

    def simulate(self):
        fig, ax = plt.subplots()

        ax.set_xlim(0, self.env_end_position.x)
        ax.set_ylim(0, self.env_end_position.y)

        def init():
            return []

        def animate(frame_number):
            self.update_velocity()

            data = []
            # print("-----------------")

            for pedestrain in self.pedestrains:

                # print(pedestrain)

                data.append(pedestrain.current_position.x)
                data.append(pedestrain.current_position.y)
                data.append(pedestrain.color)

            animlist = plt.plot(*data,marker='o',markersize=default_config.DOT_MARKER_SIZE)
            return animlist

        ani = animation.FuncAnimation(fig, animate,
                                      interval=default_config.ANIMATION_INTERVAL, init_func=init,
                                      blit=True)
        plt.show()

    def update_velocity(self):
        for i in range(0, self.total_pedestrains):

            if(self.pedestrains[i].reach_goal): continue

            total_force = Vector(0,0)

            for j in range(0, self.total_pedestrains):
                if (i == j or self.pedestrains[j].reach_goal): continue

                total_force = total_force.add(self.pedestrains[i].calculate_rforce_from_neighbour(self.pedestrains[j]))



            new_velocity = self.pedestrains[i].update_velocity(total_force)

            if (new_velocity.get_length() >= default_config.DEFAULT_MAX_SPEED):
                new_velocity = new_velocity.normalize_by(self.pedestrains[i].target_position)
                new_velocity = new_velocity.multiply_by(default_config.DEFAULT_MAX_SPEED)

            self.pedestrains[i].set_actual_velocity(new_velocity)

            self.pedestrains[i].update_position()

simulator_env_start_position = Vector(0,0)
simulator_env_end_position = Vector(1000,500)
total_pedestrains = 200
simulator = Simulator(total_pedestrains, simulator_env_start_position, simulator_env_end_position)
simulator.simulate()