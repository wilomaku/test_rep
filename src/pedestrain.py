import default_config
from vector import Vector
import math
# commit
class Pedestrain(object):

    def __init__(self, start, target, id, color):
        self.id = id
        self.color = color
        self.current_position = start
        self.target_position = target
        self.radius = default_config.DEFAULT_RADIUS
        self.mass = default_config.DEFAULT_MASS
        self.desired_speed = default_config.DEFAULT_DESIRED_SPEED
        self.relaxation_time = default_config.DEFAULT_RELAXATION_TIME
        self.reach_goal = False

        self.actual_velocity = Vector(1,1)
        self.direction = self.current_position.normalize_by(self.target_position)
        self.desired_velocity = self.direction.multiply_by(self.desired_speed)


    def calculate_rforce_from_neighbour(self, neighbour):

        self.direction = self.current_position.normalize_by(self.target_position)
        self.desired_velocity = self.direction.multiply_by(self.desired_speed)

        friction = (self.desired_velocity.subtract(self.actual_velocity)).divide_by(self.relaxation_time)
        social_force = self.cal_social_force(neighbour)

        total_force = friction.add(social_force)

        return total_force


    def cal_social_force(self, neighbour):
        A = self.mass * self.desired_speed / self.relaxation_time
        R = 0.52

        diff = self.current_position.subtract(neighbour.current_position)
        diff_length = diff.get_length()
        force_multiplier = A * math.exp(-1.0*diff_length*diff_length/(R*R)) * self.has_social_force_from(neighbour)
        diff_normalize = self.current_position.normalize_by(neighbour.current_position)

        social_force = diff_normalize.multiply_by(force_multiplier)

        return social_force

    def has_social_force_from(self, neighbour):
        diff = self.current_position.subtract(neighbour.current_position)
        diff_length = diff.get_length()

        dot_product = diff.dot_product(self.actual_velocity)
        angle = dot_product/(diff_length*self.actual_velocity.get_length())

        return 0.0 if angle<math.cos(40.0*math.pi/180.0) else 1.0

    def update_velocity(self, acceleration):
        self.actual_velocity = self.actual_velocity.add(acceleration)
        return self.actual_velocity

    def set_actual_velocity(self,velocity):
        self.actual_velocity = velocity

    def update_position(self):
        self.current_position = self.current_position.add(self.actual_velocity)
        self.check_reach_goal()

    def check_reach_goal(self):

        if( self.color=='b' and self.current_position.x>self.target_position.x):
            self.reach_goal = True
        elif( self.color=='r' and self.current_position.x<self.target_position.x):
            self.reach_goal = True

        if(self.reach_goal):
            print("Particle {} reach it's goal".format(self.id))
            print(self)

    def __str__(self):
        particle_info ="""Particle Id: {}
Type: {}
Current Position: {}
Target: {}
Reach Goal: {}
Current Velocity: {}
Desired Velocity: {}
Mass: {}
Radius: {}""".format(self.id,
                     self.color,
                     str(self.current_position),
                     str(self.target_position),
                     self.reach_goal,
                     self.actual_velocity,
                     self.desired_velocity,
                     self.mass,
                     self.radius)

        return particle_info
