import sys
sys.path.append('./src')

from pedestrain import Pedestrain
from vector import Vector
import default_config

def test_pedestrian_interaction():
    '''
    Test if interaction between pedestrians is as expected
    '''
    #Set initial positions
    vect_A = Vector(0,0)
    vect_B = Vector(50,0)

    pedestrian_A = Pedestrain(vect_A, vect_B, 1, 'b')
    pedestrian_B = Pedestrain(vect_B, vect_A, 2, 'r')
    
    #Get interaction space from class space
    fl_interaction = pedestrian_A.has_social_force_from(pedestrian_B)

    assert fl_interaction == False

def test_pedestrian_force():
    '''
    Test force between pedestrians
    '''
    #Set initial positions
    vect_A = Vector(0,0)
    vect_B = Vector(50,0)

    pedestrian_A = Pedestrain(vect_A, vect_B, 1, 'b')
    pedestrian_B = Pedestrain(vect_B, vect_A, 2, 'r')
    
    #Get interaction space from class space
    force = pedestrian_A.calculate_rforce_from_neighbour(pedestrian_B)

    assert force.x == -4.68
    assert force.y == -2.0
